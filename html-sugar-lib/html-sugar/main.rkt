#lang curly-fn racket/base

(provide (all-defined-out)
         (all-from-out "attributes.rkt"
                       "elements.rkt"
                       #;"styling.rkt"))

(require racket/list
         racket/string
         racket/function
         pointless
         "elements.rkt"
         "attributes.rkt"
         #;"styling.rkt"
         (except-in predicates
                    if?
                    when?
                    unless?)
         (for-syntax racket/base
                     pointless
                     (except-in predicates
                                if?
                                when?
                                unless?)))

#;(provide html-element-attributes
           basic-html-elements
           css-styling-class
           define/span
           define/styled
           define/with-style

           html
           head title
           body
           div span nav p
           h1 h2 h3 h4 h5 h6
           ul li

           link
           img
           a)


#|
;; (define/styled (header content)
;;   '(h2 bg-purple off-white)
;;   (div #:class "header" content))
;;
;; gives you (header-style) as a css style
;; and lets you use (header "stuff") as
;;   (div #:class "h2 bg-purple off-white header" "stuff")
(define-syntax (define/styled stx)
  (syntax-case stx ()
    [(_ (a-name . args)
        new-style
        (base-type body ...))
     (with-syntax

       ;; header-style
       ([style-name (~> #'a-name
                        syntax-e
                        symbol->string
                        #{format "~a-style"}
                        string->symbol
                        #{datum->syntax #'a-name % #'a-name})])

       #'(begin
           ;; (css-styling-class header-style '(h2 bg-purple off-white))
           (css-styling-class style-name new-style)
           (define (a-name #:class [a-class '()]
                           #:id [a-id '()]
                           .  args)
             (base-type
              #:class (style-name a-class)
              #:id a-id
              body ...))))]

    [(_ (a-name . args)
        new-style
        (base-type body ... . bodys))
     (with-syntax

       ;; header-style
       ([style-name (~> #'a-name
                        syntax-e
                        symbol->string
                        #{format "~a-style"}
                        string->symbol
                        #{datum->syntax #'a-name % #'a-name})])

       #'(begin
           ;; (css-styling-class header-style '(h2 bg-purple off-white))
           (css-styling-class style-name new-style)
           (define (a-name #:class [a-class '()]
                           #:id [a-id '()]
                           .  args)
             (apply base-type
                    #:class (style-name a-class)
                    #:id a-id
                    body ... bodys))))]))


;; (define/with-style (italic-header content)
;;   (header-style 'i)
;;   (header content))
;;
;; (italic-header "slanted stuff")
;; -> (div #:class "h2 bg-purple off-white i header" "slanted stuff")
(define-syntax (define/with-style stx)
  (syntax-case stx ()
    [(_ (a-name . args)
        (style-name additional-element ...)
        (base-type body ... . bodys))
     #'(define (a-name #:class [a-class '()]
                       #:id [a-id '()]
                       . args)
         (apply base-type
                #:class (style-name additional-element ... a-class)
                #:id a-id
                body ... bodys))]))
|#


;; required to make the next one work
(basic-html-elements head body
                     title
                     span
                     p
                     h1 h2 h3 h4 h5 h6)

(html-element-attributes (a href)
                         (link rel type href)
                         (img src)
                         (meta charset name content)
                         (script type src))


;; ;; just a simple macro for eg. (bold "TEXT") or (italic "text")
;; (define-syntax (define/span stx)
;;   (syntax-case stx ()
;;     [(_ (a-name class/es ...))
;;      #'(define/styled (a-name . text)
;;          `(,class/es ...)
;;          (span . text))]))

(define TACHYONS "https://unpkg.com/tachyons/css/tachyons.min.css")
(define BASSCSS "https://unpkg.com/basscss/css/basscss.min.css")
(define BASSCSS-ACE "https://unpkg.com/ace-css/css/ace.min.css")

(define (html #:title [title "written with html-sugar"]
              #:stylesheet [stylesheet/s null]
              #:script [script/s null]
              #:meta [meta/s null]
              #:no-sugar [no-sugar? #f]
              #:head [custom-head null]
              . body)
  (define (head-elems elem/s ->type)
    (~> elem/s
        (if? list? (map~> ->type)
             (λ~> (if? symbol?
                       symbol->string)
                  ->type
                  list))))
  (define ((->link rel type) href)
    (link #:rel rel #:type type #:href href))
  (define ->stylesheet (->link "stylesheet" "text/css"))
  (define stylesheet-elems (head-elems stylesheet/s ->stylesheet))
  (define (->script src)
    (script #:src src #:type "text/javascript"))
  (define script-elems (head-elems script/s ->script))
  (if no-sugar?
      `(html ,@body)
      `(html
        (head (title ,title)
              ,@stylesheet-elems
              ,@script-elems
              ,@meta/s
              ,@custom-head)
        (body
         ,@body))))
