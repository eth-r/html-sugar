#lang scribble/manual
@require[@for-label[html-sugar
                    racket/base]]

@title{html-sugar}
@author{promethea Ow0}

@defmodule[html-sugar]

A package for writing prettier html templates.

Uses macros to define each element as a function which takes keyword arguments matching the possible
attributes.

@racketblock{
#lang kw-colon racket

(require html-sugar)

(a href: "/home"
   id: 'homepage-link
   "go back to homepage")
}

Symbols and strings containing only ascii characters are valid attributes. In addition, lists of
either will be parsed into strings separated by spaces; nested lists are flattened.

In addition, you can define css-styling-classes for easier styling with Tachyons.

@racketblock{
(css-styling-class pretty '(off-white bg-purple))

(p class: (pretty 'i)
   "This text is in italics, with an off-white font color and purple background.")

(div class: pretty
     "This div is still pretty, but not in italics.")
}

These are actually lambdas which expand to their contained attributes upon application. Additionally,
any other function satisfying the same requirement (when applied without arguments, produces valid
html attributes) is acceptable. If you wish to manipulate the attributes manually for example, by
creating separate @racket{(bg 'green) (fg 'white)} functions for appending correct identifiers to
colors, or otherwise helping memorize the admittedly obscure syntax of Tachyons, you have full
access to the internals.

Furthermore, as these are functions instead of macros, you can pass them around in your own code in
completely arbitrary ways for maximum convenience.
