html-sugar
==========

A library for prettier programming of html xexprs, for use with
[html-writing](https://www.neilvandyke.org/racket/html-writing/)
or [html-template](https://www.neilvandyke.org/racket/html-template/),
and including special features for functional CSS.

```racket
(html #:title "HTML-sugar"
      #:stylesheet TACHYONS
      #:script "/path/to/script.js"
 (h1 "Welcome to my website")
 (navbar (home) (faq) (packages) (contact))
 (p #:class (fancy 'i "purple")
  "You can use any of the following for attributes:"
  (br)
  (my-list #:class my-list-style
   "strings"
   "symbols"
   "attribute-lambdas (procedures that produce valid attributes)"
   "lists containing valid attributes")
  "For example:"
  (p #:class (list pretty
                   (complex 'nested
                            '("example of" 'attributes)
                            "and"
                            custom-style
                            's))
   "extremely formatted text")
  "In the above, assuming the styling classes \"pretty\", "
  "\"complex\", and \"custom-style\" expand just to themselves, "
  "the result would look something like this:"
  (codeblock
   "'(p (@ (class "pretty complex nested example of attributes and custom-style s"))"
   "   \"extremely formatted text\")"))
```

Not all of the above functionality is implemented yet, but this is the spec.

## Attributes

Attributes are universally implemented as keyword arguments, and elements as
functions. Most importantly, as elements are functions instead of macros or
special syntax, you can easily pass them around and compose them. For example,

```racket
(p #:id 'example-paragraph
   #:class "regular"
 "this paragraph")
```

renders into the following xexpr:

```racket
'(p (@ (class "regular")
       (id "example-paragraph"))
  "this paragraph")
```

which can then be handled with all of Racket's list manipulation tools.

Html-sugar seeks to provide all commonly used html elements and their valid
attributes as pre-packaged functions. In addition it provides macros you can
use to define your own.

## Styling

Html-sugar is especially designed for easy styling with functional css toolkits
such as [tachyons](https://tachyons.io) or [basscss](https://basscss.com).

The most popular ones (that I can think of) are exported as the constants
`TACHYONS`, `BASSCSS`, and `BASSCSS-ACE` for easy inclusion in your projects.

In addition, html-sugar provides automatic handling of most ways a reasonable
person would want to write atomic css. Instead of being limited to strings,
elements' attributes can be written with symbols, lists, etc. to ensure that
whatever *feels right* will *probably* work, too.

Additionally, you can use `(css-styling-class name style)` to define new
styles using the same semantics as the attribute parsing. These styles can be
passed to elements as either themselves (`(p #:class pretty "text")`), or
they can be called as functions on any amounts of valid attributes as arguments.

(In addition, any function producing valid attributes when called with attributes
as arguments, or without arguments, will work, in case you want to do something
I haven't anticipated.)

## Templates

Because every responsible web designer includes certain information in the
`head` of their html, html-sugar assumes you're going to be one and provides
the function `(html . body)` which lets you use simple keyword arguments for
these. Html-sugar knows that most of the time you want to use the basic features
and it doesn't force you to write boilerplate for them, instead skipping
straight to the body after the keyword arguments.

For when you want to do something unexpected, you can use `#:no-sugar #t` which
doesn't provide any of the pre-written code, or `#:head` which lets you splice
a list of arbitrary extra elements into the predefined `head`.

## Convenience macros

### define/styled

Sometimes you want to create elements that always have a particular style.
`define/styled` is what you make those `(pretty-button "custom button")`s with.

In addition, using `define/styled` provides you the style as a separate styling
class so you can make anything *else* use `pretty-button-style` just as well.
Isn't technology amazing?

### define/with-style

Will do the same as `define/styled`, except without providing a new style.

### define/span

`(define/span name style...)` will define `name` as a span with the class
`style`; useful for defining custom styling rules in your html.

## License

As usual, do not sue anybody over this, and ye shall not be sued. Do publish
your derivative works with the Do Not Sue Anybody License, though.

## Notes

The library is written almost completely in
[pointless Racket](https://gitlab.com/promethea_o/pointless).
